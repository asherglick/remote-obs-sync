#include <obs-module.h>
#include <obs-frontend-api.h>
#include "InfoWriter.h"

// The name of the source to add
const char* cc_network_sync_idname = "CC Network Sync";

// A filter for the file browse menu for files
const char *recording_format = "recording_fromat";
const char *exclude_spaces = "exclude_spaces";
const char *recording_folder = "recording_folder";
const char *network_sync_id = "network_sync_id";


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// HOTKEY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void cc_network_sync_hotkey_custom_marker(void *data, obs_hotkey_id id, obs_hotkey_t *hotkey, bool pressed) {
   UNUSED_PARAMETER(id);
   UNUSED_PARAMETER(hotkey);

   if (pressed) {
      InfoWriter *Writer = static_cast<InfoWriter *>(data);
      // Call the writer function with an integer representing the saved value for the thing...
      Writer->WriteToFile("Custom Marker");
   }
}

void cc_network_sync_hotkey_episode_marker(void *data, obs_hotkey_id id, obs_hotkey_t *hotkey, bool pressed) {
   UNUSED_PARAMETER(id);
   UNUSED_PARAMETER(hotkey);

   if (pressed) {
      InfoWriter *Writer = static_cast<InfoWriter *>(data);
      // Call the writer function with an integer representing the saved value for the thing...
      Writer->WriteToFile("Episode Marker");
   }
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Other function
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_get_name
//
// A helper funciton for obs-studio to return the name of the pluigin
////////////////////////////////////////////////////////////////////////////////
const char* cc_network_sync_get_name(void *type_data) {
   UNUSED_PARAMETER(type_data);

   return cc_network_sync_idname;
}


////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_frontend_event_callback
//
// This function is a callback for whenever a frontend eventis triggered.
// It handles calling any relevent functions for when a frontend event occurs
// TODO: This complicated of a function is not needed. We should move the logic
//       out of the info writer and into this function to only react on record
//       start or record stop events instead of all kinds of events
////////////////////////////////////////////////////////////////////////////////
void cc_network_sync_frontend_event_callback(enum obs_frontend_event event, void *private_data)
{
   InfoWriter *logger = static_cast<InfoWriter *>(private_data);

   if (event == OBS_FRONTEND_EVENT_RECORDING_STARTED) {
      logger->MarkStart();
   }
   else if (event == OBS_FRONTEND_EVENT_RECORDING_STOPPED) {
      logger->MarkStop();
   }
}


////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_source_create
//
// A function that is called on the creation of the a source to the tree
// This function handles creating the hotkey settings UI in the OBS settings menu
////////////////////////////////////////////////////////////////////////////////
void* cc_network_sync_source_create(obs_data_t *settings, obs_source_t *source)
{
   InfoWriter *Writer = new InfoWriter();

   UNUSED_PARAMETER(settings); // Not sure what this does

   // Register a hotkey UI element that will call a given function
   obs_hotkey_register_source(source, "CCNetworkSync.CustomMarker", "Add Custom Marker", cc_network_sync_hotkey_custom_marker, Writer);
   obs_hotkey_register_source(source, "CCNetworkSync.EpisodeMarker", "Add Episode Start / End Marker", cc_network_sync_hotkey_episode_marker, Writer);

   // Configure a callback for any frontend events
   obs_frontend_add_event_callback(cc_network_sync_frontend_event_callback, Writer);

   return Writer;
}


////////////////////////////////////////////////////////////////////////////////
// Configure the settings that this plugin will have on the source
//
// Recording Folder - Folder Path
// Generate File Name without space - Checkbox
// Format - String
// Sync Signal Number - String
////////////////////////////////////////////////////////////////////////////////
obs_properties_t* cc_network_sync_properties(void *unused) {
   UNUSED_PARAMETER(unused);

   obs_properties_t *props = obs_properties_create();

   obs_properties_add_path(props, recording_folder, obs_module_text("Recording Folder"), OBS_PATH_DIRECTORY, NULL, NULL);
   obs_properties_add_bool(props, exclude_spaces, obs_module_text("Generate File Name without Space"));
   obs_properties_add_text(props, recording_format, obs_module_text("Format"), OBS_TEXT_DEFAULT);
   obs_properties_add_text(props, network_sync_id, obs_module_text("Network Sync ID"), OBS_TEXT_DEFAULT);
   return props;
}


////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_get_defaults
//
// This function sets up the default values for all of the settings that this
// plugin uses
////////////////////////////////////////////////////////////////////////////////
void cc_network_sync_get_defaults(obs_data_t *settings) {
   obs_data_set_default_string(settings, recording_folder, "/tmp");
   obs_data_set_default_bool(settings, exclude_spaces, false);
   obs_data_set_default_string(settings, recording_format, "%CCYY-%MM-%DD %hh-%mm-%ss");
   obs_data_set_default_string(settings, network_sync_id, "0000000000000000");
}


////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_update
//
// This function gets called whenever the settings are updated. It allows the
// plugin to refresh the settings that it has moved into other datastructures
////////////////////////////////////////////////////////////////////////////////
void cc_network_sync_update(void *data, obs_data_t *settings) {
   InfoWriter *Writer = static_cast<InfoWriter *>(data);

   const char *_recording_folder = obs_data_get_string(settings, recording_folder);
   const bool _exclude_spaces = obs_data_get_bool(settings, exclude_spaces);
   const char *_recording_format = obs_data_get_string(settings, recording_format);
   const char *_network_sync_id = obs_data_get_string(settings, network_sync_id);

   InfoWriterSettings* WriterSettings = Writer->GetSettings();

   WriterSettings->SetRecordingFolder(_recording_folder);
   WriterSettings->SetExcludeSpaces(_exclude_spaces);
   WriterSettings->SetFileNameFormat(_recording_format);
   WriterSettings->SetNetworkSyncID(_network_sync_id);

}

////////////////////////////////////////////////////////////////////////////////
// An unused function helper for general obs plugins, returns 0
////////////////////////////////////////////////////////////////////////////////
uint32_t cc_network_sync_get_width(void* data) {
   UNUSED_PARAMETER(data);
   return 0;
}

////////////////////////////////////////////////////////////////////////////////
// An unused funciton helper for general obs plugins, returns 0
////////////////////////////////////////////////////////////////////////////////
uint32_t cc_network_sync_get_height(void* data) {
   UNUSED_PARAMETER(data);
   return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Called when the plugin is destroyed, cleans up extra data left behind
////////////////////////////////////////////////////////////////////////////////
void cc_network_sync_destroy(void* data)
{
   InfoWriter *Writer = static_cast<InfoWriter *>(data);
   if (Writer != nullptr)
   {
      if (Writer->HasStarted())
      {
         Writer->MarkStop();
      }

      obs_frontend_remove_event_callback(cc_network_sync_frontend_event_callback, Writer);

      delete Writer;
   }
}


////////////////////////////////////////////////////////////////////////////////
// cc_network_sync_setup
//
// This function creates an registers the plugin by assocaited all the functions
// above with various atributes OBS Studio requires in order for the plugin to work
////////////////////////////////////////////////////////////////////////////////
struct obs_source_info cc_network_sync_source;
OBS_DECLARE_MODULE()
OBS_MODULE_USE_DEFAULT_LOCALE("ccnetworksync", "en-US")

// Function called by OBS on load
bool obs_module_load(void) {
   cc_network_sync_source.id = cc_network_sync_idname;
   cc_network_sync_source.type = OBS_SOURCE_TYPE_INPUT;
   cc_network_sync_source.get_name = cc_network_sync_get_name;
   cc_network_sync_source.create = cc_network_sync_source_create;
   cc_network_sync_source.destroy = cc_network_sync_destroy;
   cc_network_sync_source.get_width = cc_network_sync_get_width;
   cc_network_sync_source.get_height = cc_network_sync_get_height;
   cc_network_sync_source.get_properties = cc_network_sync_properties;
   cc_network_sync_source.update = cc_network_sync_update;
   cc_network_sync_source.load = cc_network_sync_update;
   cc_network_sync_source.get_defaults = cc_network_sync_get_defaults;

   obs_register_source(&cc_network_sync_source);
   return true;
}

// Function called by obs on unload
void obs_module_unload(void) {}
