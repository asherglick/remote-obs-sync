#pragma once

#include <string>

////////////////////////////////////////////////////////////////////////////////
// InfoWriterSettings
//
// A simple class that stores the settings for the info writer and that
// creates output from those settings useful for logging
////////////////////////////////////////////////////////////////////////////////
class InfoWriterSettings {
  protected:
    std::string recording_folder;
    bool exclude_spaces;
    std::string recording_format;
    std::string network_sync_id;


  public:
    // Simple Getters
    std::string GetFilename() const;
    std::string GetNetworkSyncID() const;

    // Simple Setters
    void SetRecordingFolder(std::string recording_folder);
    void SetExcludeSpaces(bool exclude_spaces);
    void SetFileNameFormat(std::string recording_format);
    void SetNetworkSyncID(std::string network_sync_id);
};
