#include "InfoWriterSettings.h"
#include <util/platform.h>

////////////////////////////////////////////////////////////////////////////////
// GetFilename
//
// Get filename will generate a full filepath of the log file based on the settings
// recording_format, exclude_spaces, and recording_folder
////////////////////////////////////////////////////////////////////////////////
std::string InfoWriterSettings::GetFilename() const {
    std::string filename = os_generate_formatted_filename("csync", !this->exclude_spaces, this->recording_format.c_str());
    std::string filepath = this->recording_folder + "/" + filename;
    return filepath;
}

std::string InfoWriterSettings::GetNetworkSyncID() const {
    return this->network_sync_id;
}



void InfoWriterSettings::SetRecordingFolder(std::string recording_folder) {
    this->recording_folder = recording_folder;
}
void InfoWriterSettings::SetExcludeSpaces(bool exclude_spaces) {
    this->exclude_spaces = exclude_spaces;
}
void InfoWriterSettings::SetFileNameFormat(std::string recording_format) {
    this->recording_format = recording_format;
}
void InfoWriterSettings::SetNetworkSyncID(std::string network_sync_id) {
    this->network_sync_id = network_sync_id;
}
