CC Network Sync
===============
CC Network Sync is an OBS plugin that is used to sync multiple recording sessions timestamps together using a remote webserver

For the time being it uses the Cooperative Counter to do this as it already had most of the functionality for it.

CC Network Sync saves a metadata `.csync` file that can also contain custom markers configured from within OBS's hotkeys


Compiling
=========
CC Network Sync can be compiled on Linux or Windows

Ubuntu
------
**Pull the repo**  
You will need to pull the repo and all the git submodules too  
`git clone --recursive ssh://git@git.makerpulse.com:19681/Asher/cc-network-sync.git`

**Build Boost**  
Ubuntu's default boost package will not compile to a shared library file (you
may get an error involving recompiling with -fPIC) in order to fix this you will
need to download and compile your own boost library  
`cd boost_1_65_1`  
`./bootstrap.sh`  
`./bjam cxxflags=-fPIC cflags=-fPIC -a link=static -j14`  

**Download OBS Source**
Download the newest version of the OBS source code from github
`wget https://github.com/jp9000/obs-studio/archive/master.zip`

**Build**  
`cmake -DLIBOBS_INCLUDE_DIR="<path_to_libobs_source>" -DBOOST_ROOT="<path_to_boost_root_directory>" -DCMAKE_INSTALL_PREFIX=/usr .. && make -j4`  
Example  
`cmake -DLIBOBS_INCLUDE_DIR="~/Downloads/obs-studio/libobs" -DBOOST_ROOT="~/Downloads/boost_1_65_1" -DCMAKE_INSTALL_PREFIX=/usr .. && make -j4`  


Windows
-------
We use travis CI to compile for windows right now
In order to do this push a new version of the code up to the public gitlab repo https://gitlab.com/asherglick/remote-obs-sync

an example of doing this would be to add the repo to your sources
`git remote add public git@gitlab.com:asherglick/remote-obs-sync.git`  
and then pushing the code  
`git push public master`  

The CI system can be found at  
https://ci.appveyor.com/project/AsherGlick/remote-obs-sync/history



OBS API Documentation
=====================
https://obsproject.com/docs/
