#include "InfoWriter.h"
#include <cstdint>
#include <cmath>
#include <fstream>
#include <iostream>
#include <ctime>

////////////////////////////////////////////////////////////////////////////////
// InfoWriter
//
// A simple constructor for the infowriter class that initilizes the
// recording_log_file path to an empty string so that a recording can be started
// and set the path correctly
////////////////////////////////////////////////////////////////////////////////
InfoWriter::InfoWriter() {
    this->recording_log_file = "";
}


////////////////////////////////////////////////////////////////////////////////
// WriteToFile
//
// A function for writing a log line to the log file. This function will also
// prepend a count of miliseconds since the recording was started to the beginning
// of the log line
////////////////////////////////////////////////////////////////////////////////
void InfoWriter::WriteToFile(const std::string Data) const {
    if (this->HasStarted()) {

        // Open the file for output and appending
        std::ofstream infofile;
        infofile.open(this->recording_log_file.c_str(), std::ios::out | std::ios::app);

        std::chrono::high_resolution_clock::time_point t_end = std::chrono::high_resolution_clock::now();

        // Write to the file
        if (infofile.is_open()) {
            infofile << std::chrono::duration<long, std::milli>(std::chrono::time_point_cast<std::chrono::milliseconds>(t_end) - std::chrono::time_point_cast<std::chrono::milliseconds>(this->recording_start_time)).count() << " " << Data << std::endl;
        }

        // Close the file
        infofile.close();
    }
}


////////////////////////////////////////////////////////////////////////////////
// MarkStart
//
// MarkStart() is called whenever an event is started. This could be a recording
// event, a streaming event, or an unknown event. If the event started is a
// recording event then we will reset the timer, create a new log file based
// on the settings provided, and create a new socketio connection to the sync timer
////////////////////////////////////////////////////////////////////////////////
void InfoWriter::MarkStart() {
    // Reset the recording clock
    this->recording_start_time = std::chrono::high_resolution_clock::now();

    // Grab the current time for a more general timestamp and write a start line
    int64_t now = std::time(nullptr);
    this->recording_log_file = this->Settings.GetFilename();
    std::string timestamp = std::asctime(std::localtime(&now));
    WriteToFile("START RECORDING @ " + timestamp);

    std::string url = "http://cooperative-counter.herokuapp.com/";
    // std::string url = "http://localhost:5000/";

    // Connect to Sync Timer
    this->connection.connect(url);

    // Send the opening Packet
    this->connection.socket()->emit("timerconnect", "/" + this->Settings.GetNetworkSyncID());

    // React to a time reset
    this->connection.socket()->on("timer reset", [&](sio::event& ev) {
        sio::message::ptr message = ev.get_message();
        std::string hash = message->get_string();
        WriteToFile("SyncMark " + hash);
    });
}


////////////////////////////////////////////////////////////////////////////////
// MarkStop
//
// MarkStop() is called whenever an event is stopped. This could be a recording
// event, a streaming event, or an unknown event. If the event stopped is a
// recording event then we will write a final line to the log and close the
// connection to socketio
////////////////////////////////////////////////////////////////////////////////
void InfoWriter::MarkStop() {
    // Write end log to sync file
    int64_t now = std::time(nullptr);
    std::string timestamp = std::asctime(std::localtime(&now));
    WriteToFile("STOP RECORDING @ " + timestamp);

    // Unset log file name
    this->recording_log_file = "";

    // Close socketio connectoin
    this->connection.close();
}


////////////////////////////////////////////////////////////////////////////////
// HasStarted
//
// A helper functoin to return if a file is currently being recorded to
////////////////////////////////////////////////////////////////////////////////
bool InfoWriter::HasStarted() const {
     return !(this->recording_log_file == "");
}


////////////////////////////////////////////////////////////////////////////////
// GetSettings
//
// GetSettings() is a helper function to return the internal settings object
////////////////////////////////////////////////////////////////////////////////
InfoWriterSettings* InfoWriter::GetSettings() {
    return &Settings;
}
