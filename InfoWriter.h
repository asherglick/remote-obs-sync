#pragma once

#include "InfoWriterSettings.h"
#include <cstdint>
#include "sio_client.h"
#include <chrono>

////////////////////////////////////////////////////////////////////////////////
// InfoWriter
//
// A class for writing logs to a file with a milliseconds precision timer for
// when each event is logged. Utlizes InfoWriterSettings to store OBS settings
// used to determine the filepath of the log file and URL of the sync timer
////////////////////////////////////////////////////////////////////////////////
class InfoWriter {
  private:
    // Settings Object
    InfoWriterSettings Settings;

    // Path of the current log file (or blank if not logging)
    std::string recording_log_file;

    // Milisecond scale clock for when the recording started
    std::chrono::high_resolution_clock::time_point recording_start_time;

    // Socketio connection to the sync timer
    sio::client connection;

  public:
    // A simple constructor
    InfoWriter();

    // Begin sync log
    void MarkStart();

    // Write a line to the log with an added timestamp in milliseconds
    void WriteToFile(const std::string Data) const;

    // End Sync Log
    void MarkStop();

    // Helper to see if MarkStart has been called without a following MarkStop
    bool HasStarted() const;

    // Helper function to get settings object
    InfoWriterSettings *GetSettings();
};
